package com.cursospring.batch.taskletreport.dto;

import lombok.Data;

@Data
public class EmployeeDTO {

    private String employeeId;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
}

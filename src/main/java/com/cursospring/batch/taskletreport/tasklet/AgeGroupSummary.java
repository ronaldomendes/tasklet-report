package com.cursospring.batch.taskletreport.tasklet;

import com.cursospring.batch.taskletreport.dto.EmployeeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.util.CollectionUtils;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cursospring.batch.taskletreport.utils.Constants.FILE_PATH;

@Slf4j
public class AgeGroupSummary implements Tasklet {

    @Override
    public RepeatStatus execute(StepContribution stepCont, ChunkContext chunk) throws Exception {
        try (Stream<String> employees = Files.lines(Paths.get(FILE_PATH))) {
            List<EmployeeDTO> dto = employees.map(data -> data.split(","))
                    .map(AgeGroupSummary::employeeMapper).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(dto)) {
                Map<Integer, Long> ageMap = dto.stream()
                        .collect(Collectors.groupingBy(EmployeeDTO::getAge, Collectors.counting()));

                log.info("Summary Result: {}", ageMap);
            }
        }
        return RepeatStatus.FINISHED;
    }

    private static EmployeeDTO employeeMapper(String... data) {
        EmployeeDTO dto = new EmployeeDTO();
        dto.setEmployeeId(data[0]);
        dto.setFirstName(data[1]);
        dto.setLastName(data[2]);
        dto.setEmail(data[3]);
        dto.setAge(Integer.parseInt(data[4]));
        return dto;
    }
}
